const express = require('express');
const app = express();

const port = 3000;

// req = request , res = response -- Le navigateur envoie une demande et le serveur envoie une réponse 
app.get('/', (req, res) => {
    res.send('hello world!');
}) 

// Définition d'une route data
app.get('/data', (req, res) => {
    res.send({name : 'Pikachu', power : 20, life : 30});
}) 

// Définition d'une route digimon, 3 objets JSON
app.get('/digimon', (req, res) => {
    res.send([{name : 'Agumon', power : 20, life : 30}, {name : 'Gabumon', power : 30, life : 25}, {name : 'Nodemon', power : ".", life : "."}])
}) 

// Démarre le serveur sur le port 3000, définit plus haut
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})